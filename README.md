This repository contains the necessary files for flashing the TWRP recovery to the Huawei Y360. This has only been tested on the U61 model but probably also works for the U03, U23 and U31.

## Instructions 
It could probably be installed using Linux, but it has been tested using the SP Flash Tool running on Windows. 

Fill in the scatter file and the key file in the SP Flash Tool. Set a checkbox on the recovery partition and browse to the TWRP file.

Turn of the phone, connect it and click on "download", a green check should appear after a while.

Enter TWRP by pushing Vol Up + Power, then navigate to recovery. 
